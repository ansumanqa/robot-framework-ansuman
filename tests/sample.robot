*** Settings ***
Documentation    Suite description
Library   SeleniumLibrary

*** Variables ***
${URL}    https://www.makemytrip.com/
${BROWSER}   Chrome

*** Test Cases ***
Test title
    [Tags]    SMOKE
    Open Browser   ${URL}    ${BROWSER}
    Close Browser


*** Keywords ***
Provided precondition
